<?php

/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
// require_once 'config.php';
require_once 'Haanga/Haanga.php';
require_once 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\HaangaView(),
    'log.enabled' => true
        /* ,     'log.writer' => new \Slim\Logger\Log4phpWriter() */
        ));

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */
// Routes
$app->get('/', 'getRoot');
$app->get('/kf/:query', 'getDataKF');

function getRoot() {
    $app = \Slim\Slim::getInstance();
    $data = array('basepath' => '');
    $app->render('index.tpl.html', $data);
}

function getDataKF($query) {
    $app = \Slim\Slim::getInstance();
    $data = array(array('id' => 1, 'value' => "Valor 1 $query"), array('id' => 2, 'value' => "Valor 2 $query"), array('id' => 3, 'value' => "Valor 3 $query"));
    $app->contentType('application/json');
    echo json_encode($data);
}

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
