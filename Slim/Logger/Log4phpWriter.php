<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Slim\Logger;

/**
 * Description of Log4phpWriter
 *
 * @author Damian
 */
class Log4phpWriter {

    public function write($object, $level) {
        require_once( 'log4php/Logger.php' );
        
        // Tell log4php to use our configuration file.
        \log4php\Logger::configure('config.xml');
       
        // Fetch a logger, it will inherit settings from the root logger
        $log = \log4php\Logger::getLogger('sistema.deudas');

        switch ($level) {
            case \Slim\Log::FATAL:
                $log->fatal($object);
                break;

            case \Slim\Log::ERROR:
                $log->error($object);
                break;

            case \Slim\Log::WARN:
                $log->warm($object);
                break;

            case \Slim\Log::INFO:
                $log->info($object);
                break;

            default:
                $log->debug($object);
                break;
        }
    }
}
