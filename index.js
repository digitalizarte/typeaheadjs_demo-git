$(document).ready(function () {
    // constructs the suggestion engine
    var data = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: '/kf/%QUERY'
    });

    // kicks off the loading/processing of `local` and `prefetch`
    data.initialize();

    $('.typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 3
    },
    {
        name: 'data',
        displayKey: 'value',
        source: data.ttAdapter()
    }).on('typeahead:selected', function (evt, suggestion, dataset) {
        $('#keyfinder-selected').val(suggestion.id);
    });
});