# Demo de uso de la librería Typeaheadjs #


Este proyecto muestra como integrar un control que al escribir sugiere opciones para completar utilizando un servicio remoto para completar la tarea. 
Esta montado sobre SlimFramework como gestor de peticiones, Haanga como gestor de templates, Bootstrap para el diseño de la interfaz web, Typeaheadjs para la gestión del control de sugerencias.

Se utilizan las siguientes librerías: 

 * Twitter Typeaheadjs https://github.com/twitter/typeahead.js
 * Twitter Bootstrap http://getbootstrap.com/
 * Bootstrap-3-Typeahead https://github.com/bassjobsen/Bootstrap-3-Typeahead
 * Haanga http://haanga.org/
 * SlimFramework http://www.slimframework.com/